(function () {
    'use strict';
    var app = angular.module('myApp', ['gameModule', 'rankingModule']);


    app.service('DataService', function () {

            this.data = [
                {
                    id: 0,
                    name: 'Zwierzęta',
                    elements: [
                        {id: 0, name: 'Płetwal błękitny'}, {id: 1, name: 'Słoń'}, {id: 2, name: 'Pingwin'},
                        {id: 3, name: 'Krab'}, {id: 4, name: 'Mrówka'}

                    ]

                },
                {
                    id: 1,
                    name: 'Rośliny',
                    elements: [{id: 0, name: 'Eukaliptus Królewski'}, {id: 1, name: 'Dąb'}, {id: 2, name: 'Tulipan'},
                        {id: 3, name: 'Koniczyna'}, {id: 4, name: 'Wolfia bezkorzeniowa'}]

                },
                {
                    id: 2,
                    name: 'Jednostki masy',
                    elements: [{id: 0, name: 'Tona'}, {id: 1, name: 'Kamień'}, {id: 2, name: 'Kilogram'}, {
                        id: 3,
                        name: 'Funt'
                    }, {id: 4, name: 'Grzywna'}]

                },
                {
                    id: 3,
                    name: 'Jednostki miary',
                    elements: [{id: 0, name: 'Rok świetlny'}, {id: 1, name: 'Mila'}, {id: 2, name: 'Kilometr'}, {
                        id: 3,
                        name: 'Metr'
                    }, {id: 4, name: 'Stopa'}]


                }

            ];
            this.selectedCategory = null;
            this.ranking = [];
            this.addToRanking = function (score) {
                this.ranking.push(score);
                this.ranking.sort(function (a, b) {
                    if (a.win == b.win)  return a.time - b.time;
                    else return b.win - a.win;
                });
            };

        }
    );

    app.controller('CategoryController', function (DataService) {

        this.list = DataService.data;
        this.setSelected = function (category) {
            DataService.selectedCategory = category;
        }

    });


})();
