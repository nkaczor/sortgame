/**
 * Created by natalia on 1/10/15.
 */
(function () {
    'use strict';
    var app = angular.module('gameModule', []);

    app.directive('gameElement', function () {
        return {
            restrict: 'E',
            templateUrl: 'game-element.html'
        }

    });
    app.controller('GameController', function ($scope, $timeout, DataService) {


        this.game = null;
        this.maxTime = 20;
        this.counter = this.maxTime;
        this.endGame = false;

        $scope.service = DataService;
        var self = this;


        $scope.$watch("service.selectedCategory", function (newVal) {
            if (newVal != null && newVal != self.game) {
                self.game = newVal;
                self.startGame();
            }
        });

        var stopped;
        this.countdown = function () {
            stopped = $timeout(function () {
                self.counter -= 0.1;
                if (self.counter < 0) {
                    self.counter = 0;
                    self.check();
                }
                else self.countdown();
            }, 100);
        };

        this.startGame = function () {
            this.stopGame(); //zakończenie gry jeśli się nie zakończyła
            this.shuffleArray(this.game.elements);
            this.counter = self.maxTime;
            this.endGame = false;
            this.countdown();
        };
        this.stopGame = function () {
            this.endGame = true;
            $timeout.cancel(stopped);
        };

        this.swapUp = function (index) {
            var tmp = this.game.elements[index];
            this.game.elements[index] = this.game.elements[index - 1];
            this.game.elements[index - 1] = tmp;
        };
        this.swapDown = function (index) {
            var tmp = this.game.elements[index];
            this.game.elements[index] = this.game.elements[index + 1];
            this.game.elements[index + 1] = tmp;
        };
        this.shuffleArray = function (array) {
            for (var i = array.length - 1; i > 0; i--) {
                var j = Math.floor(Math.random() * (i + 1));
                var temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
            return array;
        }
        this.check = function () {
            this.stopGame();

            this.win = true;
            for (var i = 0; i < this.game.elements.length; i++) {
                if (this.game.elements[i].id != i) {
                    this.win = false;
                    break;
                }
            }

            DataService.addToRanking({win: this.win, time: this.maxTime - this.counter});

        };

    });

})();
