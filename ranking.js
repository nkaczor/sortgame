/**
 * Created by natalia on 1/10/15.
 */

(function () {
    'use strict';
    var app = angular.module('rankingModule', []);


    app.directive('ranking', function () {
        return {
            controller: function (DataService) {
                this.ranking = DataService.ranking;
            },
            controllerAs: 'rankingCtrl',
            restrict: 'E',
            templateUrl: 'ranking.html'
        }

    });


})();
